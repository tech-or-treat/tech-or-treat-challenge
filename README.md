# Happy Tech or Treat! 

## Getting started

Welcome to the GitLab Tech-or-Treat Code Challenge! `Were so glad your here`. Hopefully you aren't too spooked by spelling errors and YAML! 

If you do not already have a GitLab.com account, please sign up [here](https://gitlab.com/users/sign_up) so you can participate.  

## Level 1

Simply fork this project into `you're` own namespace to get `starter`.

Once that is `don't`, fix the `speeling` errors or other typos in this `READYOU.md` using the built- in (WebIDE)[https://docs.gitlab.com/ee/user/project/web_ide/]. From the WebIDE, make a [merge request](https://gitlab.com/tech-or-treat/tech-or-treat-challenge/-/merge_requests) to your forked repository with the updated README. 

Comment on your new merge request saying "@taylor - Tech-or-Treat!" Please make sure you are tagging Taylor Carr. 

## Level 2

For level two, check out our [Static Application Security Testing (SAST) Documentation](https://docs.gitlab.com/ee/user/application_security/sast/). Use the documentation to manually add the SAST scanner to your forked project's gitlab-ci.yml file. 

Hint: you are going to be opening another merge request! Comment on your new merge request saying "@taylor - Tech-or-Treat!" Please make sure you are tagging Taylor Carr. 

## Level 3

For level 3, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to: The Ruby on Rails backend, the Vue-based frontend, the Go-based services like the GitLab Runner and Gitaly, and the documentation for all of those things and more.
